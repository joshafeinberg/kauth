package com.joshafeinberg.kauth.repositories

import com.joshafeinberg.kauth.models.AccessToken
import com.joshafeinberg.kauth.models.RefreshToken

/**
 * An interface to define needed methods to help with token validation
 */
interface KAuthRepository {

    /**
     * check to see if a token has already been used and is currently active
     * @param token the potential token
     * @return if the token is used
     */
    fun getTokenUsed(token: String): Boolean

    /**
     * inserts the newly generated token
     * @param userId the unique user identifier
     * @param accessToken the new access token
     * @param refreshToken the new refresh token, can be null if turned off
     * @param expiresIn the expiration time, can be null if turned off
     */
    fun insertToken(
        userId: Int,
        accessToken: AccessToken,
        refreshToken: RefreshToken?,
        expiresIn: Long?
    ): Boolean

    /**
     * expires the old refresh token when a user logs in with it and is provided a new token
     * @param refreshToken the old refresh token
     */
    fun expireOldToken(refreshToken: RefreshToken)

    /**
     * expires all tokens before the time passed in
     * @param timeBefore the time to expire all tokens before
     * @return the number of tokens expired
     */
    fun expireOldTokens(timeBefore: Long): Int

    /**
     * returns the user identifier for an access token if one exists
     * @param accessToken the access token
     * @return the unique user identifier or null if it cannot be found
     */
    fun findUserIdByAccessToken(accessToken: AccessToken): Int?

    /**
     * returns the user identifier for a refresh token if one exists
     * @param refreshToken the refresh token
     * @return the unique user identifier or null if it cannot be found
     */
    fun findUserIdByRefreshToken(refreshToken: RefreshToken): Int?
}