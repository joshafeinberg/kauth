package com.joshafeinberg.kauth.models

/**
 * An inline class to represent an access token
 *
 * @property s the access token string
 */
inline class AccessToken(val s: String)