package com.joshafeinberg.kauth.models

/**
 * A wrapper around the success or failure of a login attempt in [com.joshafeinberg.kauth.KAuthController.handleLogin]
 */
sealed class LoginResult {

    /**
     * A successful login attempt
     *
     * @property token the newly generated token
     */
    class Success(val token: Token) : LoginResult()

    /**
     * A failure login attempt
     */
    object Failure : LoginResult()

}