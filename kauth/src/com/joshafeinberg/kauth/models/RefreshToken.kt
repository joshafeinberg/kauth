package com.joshafeinberg.kauth.models

/**
 * An inline class to represent a refresh token
 *
 * @param s the refresh token string
 */
inline class RefreshToken(val s: String)