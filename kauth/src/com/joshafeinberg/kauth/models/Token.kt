package com.joshafeinberg.kauth.models

import com.google.gson.annotations.SerializedName

/**
 * The base token result returned from logging in
 *
 * @property accessToken the newly generated access token
 * @property tokenType the type of token, by default is "bearer"
 * @property expiresIn how long until the token expires, nullable based on [com.joshafeinberg.kauth.KAuthConfiguration.tokenExpiration]
 * @property refreshToken the newly generated refresh token, nullable based on [com.joshafeinberg.kauth.KAuthConfiguration.useRefreshToken]
 * @property createdAt a unix timestamp of when the token was created
 */
open class Token(
    @SerializedName("access_token") val accessToken: AccessToken,
    @SerializedName("token_type") val tokenType: String,
    @SerializedName("expires_in") val expiresIn: Long?,
    @SerializedName("refreshToken") val refreshToken: RefreshToken?,
    @SerializedName("created_at") val createdAt: Long
)