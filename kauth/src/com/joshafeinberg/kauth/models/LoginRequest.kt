package com.joshafeinberg.kauth.models

import com.google.gson.annotations.SerializedName

/**
 * An object that is parsed to login a user
 *
 * @property email the users provided email (can be null if a refresh token is provided)
 * @property password the users provided password (can be null if a refresh token is provided)
 * @property refreshToken the users provided refresh token (can be null if an email and password is provided)
 * @property grantType a flag of either [GRANT_TYPE_PASSWORD] or [GRANT_TYPE_REFRESH_TOKEN]
 */
class LoginRequest(var email: String? = null,
                   var password: String? = null,
                   @SerializedName("refresh_token") var refreshToken: RefreshToken? = null,
                   @SerializedName("grant_type")  var grantType: String) {

    companion object {
        /**
         * User is logging in via email and password
         */
        val GRANT_TYPE_PASSWORD = "password"
        /**
         * User is logging in via refresh token
         */
        val GRANT_TYPE_REFRESH_TOKEN = "refreshToken"
    }

}