package com.joshafeinberg.kauth

import com.joshafeinberg.kauth.models.AccessToken
import com.joshafeinberg.kauth.repositories.KAuthRepository
import io.ktor.application.call
import io.ktor.auth.*
import io.ktor.http.HttpStatusCode
import io.ktor.request.ApplicationRequest
import io.ktor.response.respond

/**
 * Represents a KAuth authentication provider
 * @param name is the name of the provider, or `null` for a default provider
 */
class KAuthAuthenticationProvider(name: String?) : AuthenticationProvider(name) {
}

/**
 * Installs KAuth Authentication mechanism
 * @param name is the name of the provider, or `null` for a default provider
 * @param kAuthRepository the repository to convert a token to a user identification
 * @param authenticatedFailedHandler optional handler for returning a body when unauthorized call fails
 */
fun Authentication.Configuration.kauth(
    name: String?,
    kAuthRepository: KAuthRepository,
    authenticatedFailedHandler: ((AuthenticationFailedCause) -> Any)? = null
) {
    val provider = KAuthAuthenticationProvider(name)

    provider.pipeline.intercept(AuthenticationPipeline.RequestAuthentication) { context ->
        val credentials = call.request.accessTokenCredentials()
        val principal: KAuthPrincipal? =
            if (credentials != null) {
                kAuthRepository.findUserIdByAccessToken(credentials)?.let {
                    KAuthPrincipal(it)
                }
            } else {
                null
            }

        val cause = when {
            credentials == null -> AuthenticationFailedCause.NoCredentials
            principal == null -> AuthenticationFailedCause.InvalidCredentials
            else -> null
        }

        if (cause != null) {
            val body = authenticatedFailedHandler?.invoke(cause)
            if (body == null) {
                call.respond(HttpStatusCode.Unauthorized)
            } else {
                call.respond(HttpStatusCode.Unauthorized, body)
            }
            return@intercept
        }
        if (principal != null) {
            context.principal(principal)
        }

    }

    register(provider)
}

private fun ApplicationRequest.accessTokenCredentials(): AccessToken? {
    val parsed = parseAuthorizationHeader()
    when (parsed) {
        is HttpAuthHeader.Single -> {
            if (!parsed.authScheme.equals("Bearer", ignoreCase = true)) {
                return null
            }
            return AccessToken(parsed.blob)
        }
        else -> return null
    }
}

private class KAuthPrincipal(val userId: Int): Principal