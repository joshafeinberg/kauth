package com.joshafeinberg.kauth.helpers

import java.security.SecureRandom

/**
 * The default token generator that simple generates a randomly generated 64 character string
 */
class DefaultTokenGenerator : TokenGenerator {

    private val random = SecureRandom()

    override fun generate(): String {
        val sb = StringBuffer()
        while (sb.length < 255) {
            sb.append(Integer.toHexString(random.nextInt()))
        }

        return sb.toString().substring(0, 64)
    }

}