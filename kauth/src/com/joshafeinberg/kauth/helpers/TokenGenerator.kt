package com.joshafeinberg.kauth.helpers

/**
 * An interface to allow creation of different implementations of generating a token string for Access and Refresh tokens
 */
interface TokenGenerator {

    /**
     * generates a string to be used as a token
     *
     * @return the new token
     */
    fun generate(): String

}

