package com.joshafeinberg.kauth

import com.joshafeinberg.kauth.helpers.DefaultTokenGenerator
import com.joshafeinberg.kauth.helpers.TokenGenerator
import com.joshafeinberg.kauth.repositories.KAuthRepository

/**
 * Configuration class for KAuth
 *
 * @property kAuthRepository a repository for handling all tokens
 * @property tokenGenerator a token generator with a default one provided
 * @property useRefreshToken if a refresh token should be returned and used to get new tokens
 * @property tokenExpiration how long until a token should be expired, can be set to null for never
 */
class KAuthConfiguration(
    val kAuthRepository: KAuthRepository,
    val tokenGenerator: TokenGenerator = DefaultTokenGenerator(),
    val useRefreshToken: Boolean = true,
    val tokenExpiration: Long? = 86400
)