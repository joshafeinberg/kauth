package com.joshafeinberg.kauth

import com.joshafeinberg.kauth.helpers.TokenGenerator
import com.joshafeinberg.kauth.models.*
import com.joshafeinberg.kauth.repositories.KAuthRepository
import java.util.concurrent.TimeUnit
import kotlin.concurrent.fixedRateTimer

/**
 * Base implementation of a controller for the KAuth implementation. Exposes a function to handle verification
 * and creation of new tokens
 *
 * @param kAuthConfiguration an instance of the configuration object
 */
abstract class KAuthController(kAuthConfiguration: KAuthConfiguration) {

    private val kAuthRepository: KAuthRepository = kAuthConfiguration.kAuthRepository
    private val tokenGenerator: TokenGenerator = kAuthConfiguration.tokenGenerator
    private val useRefreshToken = kAuthConfiguration.useRefreshToken
    private val expiresIn: Long? = kAuthConfiguration.tokenExpiration

    init {
        fixedRateTimer(
            name = "token_expiration",
            daemon = true,
            period = TimeUnit.MINUTES.toMillis(15)
        ) {
            val expireOldTokens = kAuthRepository.expireOldTokens(System.currentTimeMillis())
            println("Expired $expireOldTokens Old Tokens")
        }
    }

    /**
     * call in a login route to return a new valid token
     *
     * @param loginRequest the login request
     * @return an object containing either the token or a failure
     */
    @Throws(IllegalAccessError::class)
    fun handleLogin(loginRequest: LoginRequest): LoginResult {
        Result
        val userId = when (loginRequest.grantType) {
            LoginRequest.GRANT_TYPE_PASSWORD -> {
                isValidUser(loginRequest.email, loginRequest.password)
            }
            LoginRequest.GRANT_TYPE_REFRESH_TOKEN -> {
                if (useRefreshToken) {
                    handleRefreshToken(loginRequest.refreshToken)
                } else {
                    null
                }
            }
            else -> null
        }

        if (userId != null) {
            val accessToken = AccessToken(generateToken())
            var refreshToken: RefreshToken? = null
            if (useRefreshToken) {
                refreshToken = RefreshToken(generateToken())
            }
            val insertToken = kAuthRepository.insertToken(userId, accessToken, refreshToken, expiresIn)
            if (insertToken) {
                return LoginResult.Success(defaultTokenResponse(accessToken, refreshToken, expiresIn))
            }
        }

        return LoginResult.Failure
    }

    /**
     * check if the user details provided is valid
     *
     * @param email the email provided
     * @param password the password provided
     * @return a unique user id
     */
    abstract fun isValidUser(email: String?, password: String?): Int?

    private fun handleRefreshToken(refreshToken: RefreshToken?): Int? {
        if (refreshToken == null) {
            return null
        }
        val userId = kAuthRepository.findUserIdByRefreshToken(refreshToken) ?: return null
        kAuthRepository.expireOldToken(refreshToken)
        return userId
    }

    private fun defaultTokenResponse(accessToken: AccessToken, refreshToken: RefreshToken?, expiresIn: Long?): Token {
        val newToken = Token(
            accessToken = accessToken,
            tokenType = "bearer",
            expiresIn = expiresIn,
            refreshToken = refreshToken,
            createdAt = System.currentTimeMillis()
        )
        return tokenResponse(newToken) ?: newToken
    }

    /**
     * allows overriding of the response when logging in
     *
     * @param token the default token response
     * @return the new response or null to use the default
     */
    open fun tokenResponse(token: Token): Token? {
        return null
    }

    private fun generateToken(): String {
        var newToken: String? = null
        while (newToken == null) {
            newToken = tokenGenerator.generate()
            if (kAuthRepository.getTokenUsed(newToken)) {
                newToken = null
            }
        }
        return newToken
    }
}