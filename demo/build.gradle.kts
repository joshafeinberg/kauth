plugins {
    application
}

dependencies {
    val ktorVersion = ext["ktor_version"]
    compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${ext["kotlin_version"]}")
    compile("io.ktor:ktor-server-core:$ktorVersion")
    compile("io.ktor:ktor-server-host-common:$ktorVersion")
    compile("io.ktor:ktor-auth:$ktorVersion")
    compile("io.ktor:ktor-server-netty:$ktorVersion")
    compile("io.ktor:ktor-locations:$ktorVersion")
    compile("org.mindrot:jbcrypt:0.4")
    compile("com.zaxxer:HikariCP:3.2.0")
    compile("org.postgresql:postgresql:42.2.5")
    compile(project(":kauth"))
    testCompile("io.ktor:ktor-server-tests:$ktorVersion")
}

application {
    mainClassName = "com.joshafeinberg.ApplicationKt"
}