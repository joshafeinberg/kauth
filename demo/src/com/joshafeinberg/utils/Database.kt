package com.tipyourself.utils

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException


class Database {

    private val hikariConfig: HikariConfig

    //private val DATABASE_URL = System.getenv("DATABASE_URL")
    private val DATABASE_URL = "postgres://solarmoo900:@localhost:5432/tipyourself"

    init {
        val credentialsAndConnectionString = DATABASE_URL.split("@")
        val credentials = credentialsAndConnectionString[0].split("postgres://")[1].split(":")
        val connectionString = credentialsAndConnectionString[1]
        hikariConfig = HikariConfig()
        hikariConfig.jdbcUrl = "jdbc:postgresql://$connectionString" // todo: ?sslmode=require IF NOT LOCAL
        hikariConfig.driverClassName = "org.postgresql.Driver"
        hikariConfig.username = credentials[0]
        hikariConfig.password = credentials[1]
    }

    private val ds = HikariDataSource(hikariConfig)

    inline fun <reified T> rawQuery(sql: String): T? {
        return getSingle(sql) {
            it.getObject(1, T::class.java)
        }
    }

    fun <T> getList(sql: String, constructor: (ResultSet) -> T): List<T> {
        var connection: Connection? = null
        var preparedStatement: PreparedStatement? = null
        var resultSet: ResultSet? = null
        val allItems: MutableList<T> = mutableListOf()
        try {
            connection = ds.connection
            preparedStatement = connection?.prepareStatement(sql)
            resultSet = preparedStatement?.executeQuery()
            resultSet?.let {
                while (it.next()) {
                    allItems += constructor(it)
                }
            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } finally {
            resultSet?.close()
            preparedStatement?.close()
            connection?.close()
        }

        return allItems
    }

    fun <T> getSingle(sql: String, constructor: (ResultSet) -> T): T? {
        return getList(sql, constructor).firstOrNull()
    }

    fun insert(insertQuery: String, values: PreparedStatement.() -> Unit): Boolean {
        var connection: Connection? = null
        var preparedStatement: PreparedStatement? = null
        try {
            connection = ds.connection
            preparedStatement = connection?.prepareStatement(insertQuery)?.apply(values)
            preparedStatement?.execute()
        }catch (ex: SQLException) {
            ex.printStackTrace()
            return false
        } finally {
            preparedStatement?.close()
            connection?.close()
        }
        return true
    }

    fun update(updateQuery: String, values: PreparedStatement.() -> Unit): Int {
        var connection: Connection? = null
        var preparedStatement: PreparedStatement? = null
        var rowsAffected = 0
        try {
            connection = ds.connection
            preparedStatement = connection?.prepareStatement(updateQuery)?.apply(values)
            rowsAffected = preparedStatement?.executeUpdate() ?: 0
        }catch (ex: SQLException) {
            ex.printStackTrace()
        } finally {
            preparedStatement?.close()
            connection?.close()
        }
        return rowsAffected
    }
}