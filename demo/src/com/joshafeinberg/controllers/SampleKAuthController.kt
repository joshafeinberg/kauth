package com.joshafeinberg.controllers

import com.joshafeinberg.kauth.KAuthConfiguration
import com.joshafeinberg.kauth.KAuthController
import com.joshafeinberg.kauth.models.Token
import com.joshafeinberg.models.LoginResponse
import com.joshafeinberg.repositories.UserRepository
import org.mindrot.jbcrypt.BCrypt

class SampleKAuthController(kAuthConfiguration: KAuthConfiguration, val userRepository: UserRepository) : KAuthController(kAuthConfiguration) {

    override fun isValidUser(email: String?, password: String?): Int? {
        if (email == null || password == null) {
            return null
        }
        val databasePassword = userRepository.getPassword(email)
        if (databasePassword != null && BCrypt.checkpw(password, databasePassword)) {
            return userRepository.byEmail(email)?.id
        }
        return null
    }

    override fun tokenResponse(token: Token): Token? {
        val user = userRepository.byToken(token.accessToken)
        return if (user != null) {
            LoginResponse(
                accessToken = token.accessToken,
                tokenType = token.tokenType,
                expiresIn = token.expiresIn,
                refreshToken = token.refreshToken,
                createdAt = token.createdAt,
                user = user
            )
        } else {
            null
        }
    }

}