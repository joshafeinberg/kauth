package com.joshafeinberg.models

import com.joshafeinberg.kauth.models.AccessToken
import com.joshafeinberg.kauth.models.RefreshToken
import com.joshafeinberg.kauth.models.Token

class LoginResponse(
    accessToken: AccessToken,
    tokenType: String,
    expiresIn: Long?,
    refreshToken: RefreshToken?,
    createdAt: Long,
    val user: User
) : Token(accessToken, tokenType, expiresIn, refreshToken, createdAt)