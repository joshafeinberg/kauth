package com.joshafeinberg.models

import com.google.gson.annotations.SerializedName
import java.sql.ResultSet

data class User(
    val id: Int,
    val email: String,
    @SerializedName(value = "first_name") val firstName: String,
    @SerializedName(value = "last_name") val lastName: String
) {

    constructor(resultSet: ResultSet) : this(
        resultSet.getInt("id"),
        resultSet.getString("email") ?: "",
        resultSet.getString("first_name") ?: "",
        resultSet.getString("last_name") ?: ""
    )
}
