package com.joshafeinberg.auth

import io.ktor.locations.Location

object KAuthRoutes {

    @Location("oauth/token")
    object Login

}