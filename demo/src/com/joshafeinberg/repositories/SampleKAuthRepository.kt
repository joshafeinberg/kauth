package com.joshafeinberg.repositories


import com.joshafeinberg.kauth.models.AccessToken
import com.joshafeinberg.kauth.models.RefreshToken
import com.joshafeinberg.kauth.repositories.KAuthRepository
import com.tipyourself.utils.Database
import java.sql.Timestamp
import java.sql.Types
import java.util.*

class SampleKAuthRepository(val database: Database) : KAuthRepository {

    private val gmtTimezone = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

    override fun getTokenUsed(token: String): Boolean {
        return database.rawQuery<Int?>("SELECT id FROM oauth_access_tokens WHERE (token = '$token' OR refreshToken = '$token') AND revoked_at is null") != null
    }

    override fun insertToken(userId: Int, accessToken: AccessToken, refreshToken: RefreshToken?, expiresIn: Long?): Boolean {
        return database.insert("INSERT INTO oauth_access_tokens " +
                "(resource_owner_id, token, refreshToken, expires_in, created_at) " +
                "VALUES " +
                "(?, ?, ?, ?, ?)") {
            setInt(1, userId)
            setString(2, accessToken.s)
            setString(3, refreshToken?.s)
            if (expiresIn != null) {
                setLong(4, expiresIn)
            } else {
                setNull(4, Types.INTEGER)
            }
            setTimestamp(5, Timestamp(System.currentTimeMillis()), gmtTimezone)
        }
    }

    override fun expireOldToken(refreshToken: RefreshToken) {
        database.update("UPDATE oauth_access_tokens SET revoked_at = ? WHERE refreshToken = ?") {
            setTimestamp(1, Timestamp(System.currentTimeMillis()), gmtTimezone)
            setString(2, refreshToken.s)
        }
    }

    override fun expireOldTokens(timeBefore: Long): Int {
        return database.update("UPDATE oauth_access_tokens SET revoked_at = ? " +
                "WHERE revoked_at is null AND ((EXTRACT(EPOCH FROM created_at at time zone 'UTC') * 1000) + (expires_in * 1000)) < ?") {
            setTimestamp(1, Timestamp(System.currentTimeMillis()), gmtTimezone)
            setLong(2, timeBefore)
        }
    }

    override fun findUserIdByAccessToken(accessToken: AccessToken): Int? {
        return database.getSingle("SELECT resource_owner_id FROM oauth_access_tokens WHERE token = '${accessToken.s}' AND revoked_at is null") {
            it.getInt("resource_owner_id")
        }
    }

    override fun findUserIdByRefreshToken(refreshToken: RefreshToken): Int? {
        return database.getSingle("SELECT resource_owner_id FROM oauth_access_tokens WHERE refreshToken = '${refreshToken.s}' AND revoked_at is null") {
            it.getInt("resource_owner_id")
        }
    }

}