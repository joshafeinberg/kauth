package com.joshafeinberg.repositories

import com.joshafeinberg.kauth.models.AccessToken
import com.joshafeinberg.models.User
import com.tipyourself.utils.Database
import java.sql.ResultSet

class UserRepository(val database: Database) {

    private val constructor: (ResultSet) -> User = { User(it) }

    fun getPassword(email: String): String? = database.rawQuery("SELECT encrypted_password FROM users WHERE email = '$email'")

    fun byEmail(email: String) = database.getSingle("SELECT * FROM users WHERE email = '$email'", constructor)

    fun byToken(accessToken: AccessToken) = database.getSingle("SELECT * " +
            "FROM users " +
            "WHERE users.id = (SELECT oauth_access_tokens.resource_owner_id FROM oauth_access_tokens " +
            "WHERE oauth_access_tokens.token = '${accessToken.s}' " +
            "LIMIT 1) " +
            "LIMIT 1", constructor)

}