package com.joshafeinberg

import com.joshafeinberg.auth.KAuthRoutes
import com.joshafeinberg.controllers.SampleKAuthController
import com.joshafeinberg.kauth.KAuthConfiguration
import com.joshafeinberg.kauth.kauth
import com.joshafeinberg.kauth.models.LoginRequest
import com.joshafeinberg.kauth.models.LoginResult
import com.joshafeinberg.repositories.SampleKAuthRepository
import com.joshafeinberg.repositories.UserRepository
import com.tipyourself.utils.Database
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.locations.post
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

const val AUTH_OAUTH_NAME = "kauth"
private val UNAUTHORIZED_BODY = mapOf("failure" to "You are not authenticated")

@KtorExperimentalLocationsAPI
fun main(args: Array<String>) {
    embeddedServer(Netty, 8080) {
        val database = Database()
        val userRepository = UserRepository(database)
        val kAuthRepository = SampleKAuthRepository(database)
        val kAuthController = SampleKAuthController(KAuthConfiguration(kAuthRepository), userRepository)

        install(Authentication)

        authentication {
            kauth(AUTH_OAUTH_NAME, kAuthRepository) { UNAUTHORIZED_BODY }
        }

        install(Locations)
        install(ContentNegotiation) { gson() }

        routing {
            authenticate(AUTH_OAUTH_NAME) {
                get("/authenticated") {
                    call.respond(mapOf("success" to "You are authenticated"))
                }
            }

            post<KAuthRoutes.Login> {
                val loginRequest = call.receive<LoginRequest>()
                val loginResult = kAuthController.handleLogin(loginRequest)
                when (loginResult) {
                    is LoginResult.Success -> call.respond(loginResult.token)
                    is LoginResult.Failure -> call.respond(HttpStatusCode.Unauthorized, UNAUTHORIZED_BODY)
                }.let { }
            }
        }
    }.start(wait = true)
}